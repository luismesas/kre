import * as ACTION_TYPE from '../constants/actionTypes';

export const login = () => ({
  type: ACTION_TYPE.LOGIN
});

export const logout = () => ({
  type: ACTION_TYPE.LOGOUT
});
