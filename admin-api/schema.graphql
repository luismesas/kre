scalar Upload

type Query {
  me: User
  users: [User!]!
  runtime(id: ID!): Runtime!
  runtimes: [Runtime!]!
  version(id: ID!): Version!
  versions(runtimeId: ID!): [Version!]!
  alerts: [Alert!]
  settings: Settings!
  userActivityList(
    userEmail: String
    type: UserActivityType
    fromDate: String
    toDate: String
    lastId: String
  ): [UserActivity!]!
}

type Mutation {
  createRuntime(input: CreateRuntimeInput!): CreateRuntimeResponse!
  createVersion(input: CreateVersionInput!): CreateVersionResponse!
  startVersion(input: StartVersionInput!): Version!
  stopVersion(input: StopVersionInput!): Version!
  publishVersion(input: PublishVersionInput!): Version!
  unpublishVersion(input: UnpublishVersionInput!): Version!
  updateSettings(input: SettingsInput!): UpdateSettingsResponse
  updateVersionConfiguration(input: UpdateConfigurationInput!): Version!
}

type Subscription {
  runtimeCreated: Runtime!
  nodeLogs(runtimeId: ID!, nodeId: ID!): NodeLog!
  versionNodeStatus(versionId: ID!): VersionNodeStatus!
}

input CreateRuntimeInput {
  name: String!
}

type CreateRuntimeResponse {
  errors: [Error!]
  runtime: Runtime
}

input CreateVersionInput {
  file: Upload!
  runtimeId: String!
}

input StartVersionInput {
  versionId: ID!
}

input StopVersionInput {
  versionId: ID!
}

input PublishVersionInput {
  versionId: ID!
  comment: String!
}

input UnpublishVersionInput {
  versionId: ID!
}

type CreateVersionResponse {
  errors: [Error!]
  version: Version!
}

type VersionNodeStatus {
  date: String!
  nodeId: String!
  status: NodeStatus!
  message: String!
}

input SettingsInput {
  authAllowedDomains: [String!]
  sessionLifetimeInDays: Int
}

type UpdateSettingsResponse {
  errors: [Error!]
  settings: Settings
}

type Error {
  code: ErrorCode!
  message: String!
}

enum ErrorCode {
  UNEXPECTED_ERROR
  VALIDATION_ERROR
  BAD_REQUEST
  INTERNAL_SERVER_ERROR
}

type User {
  id: ID!
  email: String!
}

type Alert {
  id: ID!
  type: AlertLevel!
  message: String!
  runtime: Runtime!
}

enum AlertLevel {
  ERROR
  WARNING
}

type ConfigurationVariable {
  key: String!
  value: String!
  type: ConfigurationVariableType!
}

enum ConfigurationVariableType {
  VARIABLE
  FILE
}

input ConfigurationVariablesInput {
  key: String!
  value: String!
}

input UpdateConfigurationInput {
  versionId: ID!
  configurationVariables: [ConfigurationVariablesInput!]!
}

type Runtime {
  id: ID!
  name: String!
  status: RuntimeStatus!
  creationDate: String!
  creationAuthor: User!
  publishedVersion: Version
}

enum RuntimeStatus {
  CREATING
  STARTED
  ERROR
}

type Edge {
  id: ID!
  fromNode: ID!
  toNode: ID!
}

type Node {
  id: ID!
  name: String!
  status: NodeStatus!
}

enum NodeStatus {
  STARTED
  STOPPED
  ERROR
}

type Workflow {
  name: String!
  nodes: [Node!]!
  edges: [Edge!]!
}

type Version {
  id: ID!
  name: String!
  description: String!
  status: VersionStatus!
  creationDate: String!
  creationAuthor: User!
  publicationDate: String
  publicationAuthor: User
  workflows: [Workflow!]!
  configurationVariables: [ConfigurationVariable!]!
  configurationCompleted: Boolean!
}

enum VersionStatus {
  STARTING
  STARTED
  PUBLISHED
  STOPPED
}

type Settings {
  authAllowedDomains: [String!]!
  sessionLifetimeInDays: Int!
}

type UserActivityVar {
  key: String!
  value: String!
}

type UserActivity {
  id: ID!
  type: UserActivityType!
  user: User!
  date: String!
  vars: [UserActivityVar!]!
}

enum UserActivityType {
  LOGIN
  LOGOUT
  CREATE_RUNTIME
  CREATE_VERSION
  PUBLISH_VERSION
  UNPUBLISH_VERSION
  START_VERSION
  STOP_VERSION
  UPDATE_SETTING
  UPDATE_VERSION_CONFIGURATION
}

type NodeLog {
  date: String!
  type: String!
  versionId: ID!
  nodeId: ID!
  podId: ID!
  message: String!
  level: String!
}
